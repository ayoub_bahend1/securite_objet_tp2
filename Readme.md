## Format des données d'entrée

Le fichier binaire doit contenir des données hexadécimales à décoder. Ces données doivent être dans un format spécifique pour être correctement traitées par le script. Consultez la documentation pour plus de détails sur le format attendu.

## Format du fichier CSV de sortie

Le fichier CSV de sortie contiendra deux colonnes : "Latitude" et "Longitude". Les coordonnées géographiques seront au format décimal, séparées par une virgule.

Exemple de contenu du fichier CSV :


```
python decode_coordinates.py fichier_binaire fichier_csv

```


Assurez-vous de remplacer `fichier_binaire` par le nom de votre fichier binaire d'entrée et `fichier_csv` par le nom que vous souhaitez donner au fichier CSV de sortie.

Par exemple :
```
python decode_coordinates.py fichier_hex.bin fichier_sortie.csv
```


3. Une fois le script exécuté, le fichier CSV contenant les coordonnées géographiques sera créé dans le même répertoire que le script.

## Format des données d'entrée

Le fichier binaire doit contenir des données hexadécimales à décoder. Ces données doivent être dans un format spécifique pour être correctement traitées par le script. Consultez la documentation pour plus de détails sur le format attendu.

## Format du fichier CSV de sortie

Le fichier CSV de sortie contiendra deux colonnes : "Latitude" et "Longitude". Les coordonnées géographiques seront au format décimal, séparées par une virgule.

Exemple de contenu du fichier CSV :

```
Latitude,Longitude
48.858844,2.294351
```