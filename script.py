import csv
import struct

def decode(fileB, fileCSV):
    # Ouvrir le fichier hexadécimal en mode lecture binaire
    with open(fileB, 'rb') as binary_file:
        # Lire les données binaires et convertir en une chaîne hexadécimale
        hex_data = binary_file.read().hex().upper()

        # Trouver les positions des occurrences de "1D"
        positions_1d = [pos for pos in range(0, len(hex_data), 2) if hex_data[pos:pos+2] == '1D']

        # Ouvrir le fichier CSV en mode écriture
        with open(fileCSV, 'w', newline='') as csv_file:
            writer = csv.writer(csv_file)
            
            csv_file.write('Latitude,Longetude' + '\n')

            # Écrire les données dans le fichier CSV
            for pos in positions_1d:
                # Extraire 3 octets après "1D" (par exemple)
                d = str(int(hex_data[pos:pos+8],16))
                s = str(int(hex_data[pos+8:pos+16],16))
                data_to_write = d[:2] + '.' + d[2:] + "," + s[:1] + '.' + s[1:]
                
                # Écrire la ligne dans le fichier CSV
                if("44000000"<d<"53000000" ):
                    csv_file.write(data_to_write + '\n')

# Utilisation du script avec le fichier d'entrée "fichier_hex.bin" et le fichier de sortie "fichier_csv.csv"
decode('4d38d6490cf188270b61c1a3e9857097_T1091_b07ca', 'file.csv')